package com.demo.registration.controller.v1

import com.demo.registration.service.MemberService
import com.demo.registration.util.RestResponseEntityExceptionHandler
import com.fasterxml.jackson.databind.ObjectMapper
import com.demo.registration.exception.RecordNotFoundException
import com.demo.registration.model.v1.MemberDto

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content

@SpringBootTest
class MemberControllerTest extends Specification{

    def memberService = Mock(MemberService)
    MockMvc mvc

    // cannot autowired, else will read from db
//    @Autowired
//    MockMvc mvc
//    @Autowired
//    MemberService memberService

    @Shared
    def validMember_1001   = new MemberDto(1001l, "lee", "ming", "lee.ming@gmail.com", new Date())
    @Shared
    def validMember_1002   = new MemberDto(1002l, "yong", "qing", "lee.ming@gmail.com", new Date())
    @Shared
    def notFound_Prefix   =  "RecordNotFoundException: "

    //@Autowired
    ObjectMapper objectMapper = new ObjectMapper();


    Map validResponse = [
            id : 1001,
            email : 'lee.ming@gmail.com',
            fistName : 'lee',
            lastName : 'ming'
    ]


    def setup() {
        mvc = MockMvcBuilders.standaloneSetup(new RestResponseEntityExceptionHandler(), new MemberController(memberService)).build()
    }

    @Unroll
    def "Test RESTful GET"() {
        given:
            println "1000 : serviceData = " + mockData.toString()
            memberService.getMemberById(memberId) >> mockData
        when:
            def result = mvc.perform(MockMvcRequestBuilders
                    .get('/members/'+memberId))
        then:
            result.andExpect(status().is(responseCode))
            result.andExpect(jsonPath('$.firstName').value(((MemberDto)mockData).getFirstName()))
        and:
        with (objectMapper.readValue(result.andReturn().getResponse().getContentAsString(), Map)) {
            it.id == ((MemberDto)mockData).getId()
            it.email == ((MemberDto)mockData).getEmail()
            it.firstName == ((MemberDto)mockData).getFirstName()
            it.lastName == ((MemberDto)mockData).getLastName()
        }
        where:
            memberId   | responseCode | mockData
            1001       | 200          | validMember_1001
            1002       | 200          | validMember_1002
    }

    @Unroll
    def "Test GET with RecordNotFound Exception"() {

        given:
            memberService.getMemberById(memberId) >> { throw new RecordNotFoundException (String.valueOf(memberId))}
        when:
            def result
            try{
                result = mvc.perform(MockMvcRequestBuilders
                        .get('/members/'+memberId))

            }catch(Exception e){
                e.getMessage().contains(mockException)
            }
        then:
            result.andExpect(status().is4xxClientError())
            result.andExpect(content().string(String.format("Record '%d' not found", memberId)))

        where:
            memberId   | mockException
            1001       | notFound_Prefix + 1001
            1002       | notFound_Prefix + 1002
    }

}
