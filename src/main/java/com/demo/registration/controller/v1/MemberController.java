package com.demo.registration.controller.v1;

import com.demo.registration.model.v1.MemberDto;
import com.demo.registration.service.MemberService;
import com.demo.registration.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("/members")
public class MemberController {
    @Autowired
    private final MemberService service;

    Logger logger = LoggerFactory.getLogger(MemberController.class);

    @Autowired
    public MemberController(MemberService service) {
        this.service = service;
    }

    @GetMapping()
    public ResponseEntity<Object> getMembers() {
        return ResponseUtil.sendOkResponse(service.getMembers());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getMember(@PathVariable @NotNull Long id) {

        logger.info("getMapping : id={}", id);
        MemberDto memberDto = service.getMemberById(id);
//        logger.info("getMapping : memberDto={}", memberDto);
        return ResponseUtil.sendOkResponse(memberDto);
    }

    @PostMapping(value = "/list")
    public ResponseEntity<Object> searchMember(@RequestBody MemberDto memberDto) {
        return ResponseUtil.sendOkResponse(service.findMembers(memberDto));
    }

    @PostMapping()
    public ResponseEntity<Object> createMember(@RequestBody MemberDto memberDto) {

        return ResponseUtil.sendOkResponse(service.createMember(memberDto));
    }

    @PatchMapping()
    public ResponseEntity<Object> updateMember(@RequestBody MemberDto memberDto) {

        return ResponseUtil.sendOkResponse(service.updateMember(memberDto));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteMember(@PathVariable @NotNull Long id) {
        service.deleteMember(id);
        return ResponseUtil.sendOkResponse("member "+id+" deleted");
    }
}
