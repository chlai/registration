package com.demo.registration.mapper;

import com.demo.registration.model.v1.MemberDto;
import com.demo.registration.model.v1.Member;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring")
public interface MemberMapper {

    //@Mapping(source = "numberOfSeats", target = "seatCount")
    Member toMember(MemberDto memberDto);
    MemberDto toMemberDto(Member member);
    Iterable<MemberDto> toMemberDto(Iterable<Member> ite);
    List<MemberDto> toMemberDto(List<Member> ite);

    void updateMember(MemberDto memberDto, @MappingTarget Member member);

}

