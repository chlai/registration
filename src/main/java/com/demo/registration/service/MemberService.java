package com.demo.registration.service;

import com.demo.registration.exception.RecordNotFoundException;
import com.demo.registration.mapper.MemberMapper;
import com.demo.registration.model.v1.Member;
import com.demo.registration.model.v1.MemberDto;
import com.demo.registration.repository.GenericSpesification;
import com.demo.registration.repository.MemberRepository;
import com.demo.registration.repository.SearchCriteria;
import com.demo.registration.repository.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MemberService {

    @Autowired
    private MemberRepository repository;
    @Autowired
    MemberMapper memberMapper;

    public List<MemberDto> findMembers(MemberDto memberDto) {

        if (memberDto != null) {
            GenericSpesification<Member> spec = new GenericSpesification<>();
            System.out.println("memberDto=" + memberDto);
            if (memberDto.getEmail() != null)
                spec.add(new SearchCriteria("email", memberDto.getEmail(), SearchOperation.EQUAL));
            if (memberDto.getFirstName() != null)
                spec.add(new SearchCriteria("firstName", memberDto.getFirstName(), SearchOperation.EQUAL));
            if (memberDto.getLastName() != null)
                spec.add(new SearchCriteria("lastName", memberDto.getLastName(), SearchOperation.EQUAL));

            return memberMapper.toMemberDto(repository.findAll(spec));
        }else{
            return new ArrayList<>();
        }

    }

    public MemberDto getMemberById(Long id){
        System.out.println("********** 1000");
        Member member = repository.findById(id)
                .orElseThrow(()-> new RecordNotFoundException(String.valueOf(id)));
        return memberMapper.toMemberDto(member);
    }

    public Iterable<MemberDto> getMembers(){
        Iterable<Member> ite = repository.findAll();
        return memberMapper.toMemberDto(ite);
    }

    public MemberDto updateMember(MemberDto memberDto){
        if (memberDto != null && memberDto.getId() != null) {
            Member member = repository.findById(memberDto.getId())
                    .orElseThrow( ()-> new RecordNotFoundException(memberDto.getId().toString()) );

            memberMapper.updateMember(memberDto, member);
            repository.save(member);
            return memberMapper.toMemberDto(member);
        }else{
            return memberDto;
        }
    }

    public MemberDto createMember(MemberDto memberDto){
        if (memberDto != null ) {

            Member member = new Member();
            memberMapper.updateMember(memberDto, member);
            repository.save(member);
            return memberMapper.toMemberDto(member);
        }else{
            return memberDto;
        }
    }
    public void deleteMember(Long id){
        if (id != null ) {
            Member member = repository.findById(id)
                   .orElseThrow( ()-> new RecordNotFoundException(String.valueOf(id)) );

            repository.delete(member);
        }
    }
}
