package com.demo.registration.repository;

import com.demo.registration.model.v1.Member;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MemberRepository extends PagingAndSortingRepository<Member, Long>,
        CrudRepository<Member, Long>, JpaSpecificationExecutor<Member> {
}
