package com.demo.registration.exception;

public class RegistrationException extends RuntimeException {

    protected String code;

    public RegistrationException(){
        super();
    }

    public RegistrationException(String message, Throwable cause, String code) {
        super(message, cause);
        this.code = code;
    }

    public RegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistrationException(String message) {
        super(message );
    }
}
