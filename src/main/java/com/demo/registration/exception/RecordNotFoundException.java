package com.demo.registration.exception;

public class RecordNotFoundException extends RegistrationException {
    public RecordNotFoundException(){
        super();
    }
    public RecordNotFoundException(String msg){
        super(msg);
    }
}
