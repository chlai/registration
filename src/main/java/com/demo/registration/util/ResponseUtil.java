package com.demo.registration.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtil {

    public static ResponseEntity<Object> sendResponse(Object obj, HttpStatus status){
        return new ResponseEntity<>(obj, status);
    }

    public static ResponseEntity<Object> sendOkResponse(Object obj){
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

//    public static ResponseEntity<Object> sendOkResponse(Object obj){
//        return new ResponseEntity<>(obj, HttpStatus.);
//    }
}
