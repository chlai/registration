-- for derby
DROP TABLE  member;
CREATE TABLE member
(
    id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1000, INCREMENT BY 1),
    first_name varchar(100) NOT NULL,
    last_name varchar(100) NOT NULL,
    email varchar(100) DEFAULT NULL,
    date_of_birth DATE DEFAULT NULL,
    PRIMARY KEY (id)
);

