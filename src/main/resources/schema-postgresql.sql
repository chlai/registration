-- for postgresql
DROP TABLE IF EXISTS member;

CREATE TABLE member
(
    id  SERIAL ,
    first_name varchar(100) NOT NULL,
    last_name varchar(100) NOT NULL,
    email varchar(100) DEFAULT NULL,
    date_of_birth DATE DEFAULT NULL,
    PRIMARY KEY (id)
);
ALTER SEQUENCE member_id_seq RESTART WITH 1000 INCREMENT BY 1;
